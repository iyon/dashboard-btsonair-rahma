<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $top_5 = Api::top_5_user_activity();
        // $user_activity = Api::user_activity();
        // $license_activated = Api::license_activated();
        // $cell_deactivated = Api::cell_deactivated();
        // $dafinci = Api::dafinci();
        // $remedy = Api::remedy();
        // $btsonair = Api::btsonair();
        // $nodin = Api::nodin();

        // dd($license_activated);
        // $label_top_5 = [];
        // $value_top_5 = [];
        // $label_license_activated = [];
        // $value_license_activated = [];
        // $label_cell = [];
        // $value_cell = [];
        // $label_user = [];
        // $value_user = [];
        // dd($top_5);
        // foreach ($top_5 as $data) {
        //     array_push($label_top_5, $data['operator']);
        //     array_push($value_top_5, $data['total']);
        // }
        // foreach ($license_activated as $data2) {
        //     array_push($label_license_activated, $data2['date']);
        //     array_push($value_license_activated, $data2['total']);
        // }
        // foreach ($cell_deactivated as $data3) {
        //     array_push($label_cell, $data3['date']);
        //     array_push($value_cell, $data3['total']);
        // }
        // foreach ($user_activity as $data4) {
        //     array_push($label_user, $data4['operator']);
        //     array_push($value_user, $data4['total']);
        // }

        return view('index');
    }
}
