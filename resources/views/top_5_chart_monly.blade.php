<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div class="row">
            <div class="card-body" style="height:200px; background-color: ">
                <div class="chart" style="height:150px; background-color: white;">
                    <canvas id="barChart2"></canvas>
                </div>
            </div>
        </div>
    </body>

    <script type="text/javascript" src="{{url('')}}/js/app.js"></script>
    <!-- AdminLTE App -->
    <script src="{{url('')}}/custom/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{url('')}}/custom/js/demo.js"></script>
    <!-- jQuery -->
    <script src="{{url('')}}/custom/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <!-- <script src="custom/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <!-- ChartJS 1.0.1 -->
    <script src="{{url('')}}/custom/chartjs-old/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="{{url('')}}/custom/fastclick/fastclick.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url:'{{url("chart_btsonair")}}',
            type:'get',
            success: function(result){
                console.log(result.hasil);

                var areaChartData = {
                    labels :result.regional,
                    datasets: [
                        {
                            label               : 'BTS ON AIR',
                            fillColor           : '#20B2AA',
                            strokeColor         : '#20B2AA ',
                            pointColor          : '#ffff',
                            pointStrokeColor    : 'rgba(0,0,0,0.5)',
                            pointHighlightFill  : '#ffff',
                            //pointHighlightStroke: 'rgba(0,0,0,0.5)',
                            data : result.hasil
                        }
                    ]
                }
                //-------------
                //- BAR CHART -
                //-------------
                var barChartCanvas                   = $('#barChart2').get(0).getContext('2d')
                var barChart                         = new Chart(barChartCanvas)
                var barChartData                     = areaChartData
                var barChartOptions                  = {
                    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                    scaleBeginAtZero        : true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines      : true,
                    //String - Colour of the grid lines
                    scaleGridLineColor      : '#0000',
                    //Number - Width of the grid lines
                    scaleGridLineWidth      : 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines  : true,
                    //Boolean - If there is a stroke on each bar
                    barShowStroke           : true,
                    //Number - Pixel width of the bar stroke
                    barStrokeWidth          : 2,
                    //Number - Spacing between each of the X value sets
                    barValueSpacing         : 5,
                    //Number - Spacing between data sets within X values
                    barDatasetSpacing       : 1,
                    //String - A legend template
                    legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                    //Boolean - whether to make the chart responsive
                    responsive              : true,
                    maintainAspectRatio     : false
                }

                barChartOptions.datasetFill = false
                barChart.Bar(barChartData, barChartOptions)
            }
        });
    });
    </script>
</html>
