<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div class="row">
            <div class="card-body" style="height:360px; background-color: ">
                <div class="chart" style="height:360px; background-color: white;">
                    <canvas id="barChart"></canvas>
                </div>
            </div>
        </div>
    </body>

    <script src="{{url('')}}/js/jquery.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url:'http://localhost/bts-rekon/api/get_rekon_total',
            type:'get',
            success: function(result){
              var regions = []
              var rekons = []
              var nonrekons = []
              var total = []
              $.each(JSON.parse(result), function(idx, obj) {
                   regions.push(obj.REGIONAL);
                   rekons.push(obj.REKON);
                   nonrekons.push(obj.NONREKON);
                   total.push(obj.TOTAL);
              });
              console.log(regions);
                var areaChartData = {
                  labels  : regions,
                  datasets: [
                    {
                      label               : 'Rekon',
                      fillColor           : '#00000',
                      strokeColor         : 'rgba(210, 214, 222, 1)',
                      pointColor          : 'rgba(210, 214, 222, 1)',
                      pointStrokeColor    : '#c1c7d1',
                      pointHighlightFill  : '#fff',
                      pointHighlightStroke: 'rgba(220,220,220,1)',
                      data                : rekons
                    },
                    {
                      label               : 'Non Rekon',
                      fillColor           : 'rgba(60,141,188,0.9)',
                      strokeColor         : 'rgba(60,141,188,0.8)',
                      pointColor          : '#3b8bba',
                      pointStrokeColor    : 'rgba(60,141,188,1)',
                      pointHighlightFill  : '#fff',
                      pointHighlightStroke: 'rgba(60,141,188,1)',
                      data                : nonrekons
                    }
                  ]
                }
                //-------------
                //- BAR CHART -
                //-------------
                var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
                var barChart                         = new Chart(barChartCanvas)
                var barChartData                     = areaChartData
                barChartData.datasets[1].fillColor   = '#000000'
                barChartData.datasets[1].strokeColor = '#00a65a'
                barChartData.datasets[1].pointColor  = '#00a65a'
                var barChartOptions                  = {
                  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                  scaleBeginAtZero        : true,
                  //Boolean - Whether grid lines are shown across the chart
                  scaleShowGridLines      : true,
                  //String - Colour of the grid lines
                  scaleGridLineColor      : '#ffffff',
                  //Number - Width of the grid lines
                  scaleGridLineWidth      : 1,
                  //Boolean - Whether to show horizontal lines (except X axis)
                  scaleShowHorizontalLines: true,
                  //Boolean - Whether to show vertical lines (except Y axis)
                  scaleShowVerticalLines  : true,
                  //Boolean - If there is a stroke on each bar
                  barShowStroke           : true,
                  //Number - Pixel width of the bar stroke
                  barStrokeWidth          : 2,
                  //Number - Spacing between each of the X value sets
                  barValueSpacing         : 5,
                  //Number - Spacing between data sets within X values
                  barDatasetSpacing       : 1,
                  //String - A legend template
                  legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                  //Boolean - whether to make the chart responsive
                  responsive              : true,
                  maintainAspectRatio     : true
                }

                barChartOptions.datasetFill = false
                barChart.Bar(barChartData, barChartOptions)
            }
                    });
    });
    </script>
</html>
