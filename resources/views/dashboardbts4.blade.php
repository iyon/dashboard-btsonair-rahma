<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div class="row">
            <div class="card-body" style="height:360px; background-color: ">
                <div class="chart" style="height:360px; background-color: white;">
                    <canvas id="barChart4"></canvas>
                </div>
            </div>
        </div>
    </body>

    <script src="{{url('')}}/js/jquery.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url:'http://localhost/bts-rekon/api/get_dismantle',
            type:'get',
            success: function(result){
              var regions = []
              var dismantles = []
              var total = []
              $.each(JSON.parse(result), function(idx, obj) {
                   regions.push(obj.REGIONAL);
                   dismantles.push(obj.DISMANTLE);
                   total.push(obj.TOTAL);
              });
              console.log(regions);
                var areaChartData = {
                    labels :regions,
                    datasets: [
                        {
                            label               : 'BTS ON AIR',
                            fillColor           : '#20B2AA',
                            strokeColor         : '#20B2AA ',
                            pointColor          : '#ffff',
                            pointStrokeColor    : 'rgba(0,0,0,0.5)',
                            pointHighlightFill  : '#ffff',
                            //pointHighlightStroke: 'rgba(0,0,0,0.5)',
                            data : dismantles
                        }
                    ]
                }
                //-------------
                //- BAR CHART -
                //-------------
                var barChartCanvas                   = $('#barChart4').get(0).getContext('2d')
                var barChart                         = new Chart(barChartCanvas)
                var barChartData                     = areaChartData
                var barChartOptions                  = {
                    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                    scaleBeginAtZero        : true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines      : true,
                    //String - Colour of the grid lines
                    scaleGridLineColor      : '#0000',
                    //Number - Width of the grid lines
                    scaleGridLineWidth      : 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines  : true,
                    //Boolean - If there is a stroke on each bar
                    barShowStroke           : true,
                    //Number - Pixel width of the bar stroke
                    barStrokeWidth          : 2,
                    //Number - Spacing between each of the X value sets
                    barValueSpacing         : 5,
                    //Number - Spacing between data sets within X values
                    barDatasetSpacing       : 1,
                    //String - A legend template
                    legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                    //Boolean - whether to make the chart responsive
                    responsive              : true,
                    maintainAspectRatio     : false
                }

                barChartOptions.datasetFill = false
                barChart.Bar(barChartData, barChartOptions)
            }
        });
    });

    </script>
</html>
