<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('')}}/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{url('')}}/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{url('')}}/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{url('')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{url('')}}/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{url('')}}/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{url('')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="{{url('')}}/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
  </nav>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{url('')}}/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Project Tracking</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a href="http://10.54.36.49/change-front/" class="nav-link">
            <i class="nav-icon fa fa-fire"></i>
              <p>Dashboard BTS On Air</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="http://10.54.36.49/dashboard-license" class="nav-link">
            <i class="nav-icon fa fa-fire"></i>
              <p>License</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="http://10.54.36.49/btsonair" class="nav-link">
            <i class="nav-icon fa fa-fire"></i>
              <p>BTS On Air</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link">
            <i class="nav-icon fa fa-fire"></i>
              <p>Nodin Integrasi</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link">
            <i class="nav-icon fa fa-fire"></i>
              <p>Nodin Rehoming</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-fire"></i>
              <p>Change</p>
          </a>
        </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

   <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-sm-12 mt-3">
            <div class="row">
              <div class="col-lg-2 col-md-2">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <div class="col-xs-12 text-right">
                <div class="huge" style="font-size:40px;" id="dafinci">0</div>
                <div style="font-size:16px;">Site</div>
                </div>
                <span class="pull-left" style="font-size:20px;" id="dafinci_change">Dafinci&nbsp;<i class="fa" style="color:green">&#xf062;</i>0</span>
                <div class="clearfix"></div>
              </div>
        </div>
      </div>
       <section class="col-lg-6">
                <div class="card gradient">
                    <div class="card-header no-border">
                        <h3 class="card-title">
                            <i class="fa fa-th mr-1"></i>
                           Count of BTS Planned (Region) 
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn bg-info btn-sm" data-widget="remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body" style="background-color: white;">
                        @include('top_5_chart')
                      </div>
                </section>

                <section class="col-lg-6">
                <div class="card gradient">
                    <div class="card-header no-border">
                        <h3 class="card-title">
                            <i class="fa fa-th mr-1"></i>
                           Count of BTS On Air (Region) 
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn bg-info btn-sm" data-widget="remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body" style="background-color: white;">
                            @include('top_5_chart_monly')
                    </div>
                </section>
          <div class="col-lg-3 col-lg-6 mt-3">
                    <div class="row">
                        <div class="col-lg-3">
                        <div class="small-box" style="height: 284px; background-color: #008B8B">
                          <div class="inner">
                            <div class="col-xs-12 text-center">
                                <div class="huge"  style="font-size:24px;" id="nodin"><h4 style="color:white">Activated Cell</h4></div>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <div class="huge" style="font-size:20px;" id="act_cell"><h1 style="color:white">0</h1></div>
                                <div class="huge" style="font-size:20px;"><h4 style="color:white">Cell</h4></div>
                            </div>
                                <div class="clearfix"></div>
                            </div>
                    </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3">
                            <!-- small box -->
                            <div class="small-box" style="height: 284px; background-color: #CD5C5C">
                            <div class="inner">
                                <div class="col-xs-12 text-center">
                                <div class="huge" style="font-size:24px;" id="nodin"><h4 style="color:white">Deactivated Cell</h4></div>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <div class="huge" style="font-size:20px;" id="dea_cell"><h1 style="color:white">0</h1></div>
                                <div class="huge" style="font-size:20px;"><h4 style="color:white">Cell</h4></div>
                            </div>
                            <div class="clearfix"></div>
                    </div>
                    </div>
                    </div>
                        <!-- ./col -->
                        <div class="col-lg-3">
                            <!-- small box -->
                            <div class="small-box" style="height: 284px; background-color: #F4A460">
                                <div class="inner">
                                <div class="col-xs-12 text-center">
                                <div class="huge" style="font-size:24px;" id="nodin"><h4 style="color:white">Active License</h4></div>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <br/>
                                <div class="huge" style="font-size:20px;" id="act_license"><h1 style="color:white">0</h1></div>
                                <div class="huge" style="font-size:20px;"><h4 style="color:white">License</h4></div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                            </div>
                        </div>

                        <!-- ./col -->
                        <div class="col-lg-3">
                            <div class="small-box" style="height: 284px; background-color: #CD5C5C">
                                <div class="inner">
                                <div class="col-xs-12 text-center">
                                <div class="huge" style="font-size:24px;" id="nodin"><h4 style="color:white">Deactivated License</h4></div>
                                <br/>
                                        <br/>
                                <br/>
                                <br/>
                                <div class="huge" style="font-size:20px;" id="dea_license"><h1 style="color:white">0</h1></div>
                                <div class="huge" style="font-size:20px;"><h4 style="color:white">License</h4></div>
                              </div>
                                <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

      <section class="col-lg-6">
                <div class="card gradient" style="margin-top: 17px;">
                    <div class="card-header no-border">
                        <h3 class="card-title">
                            <i class="fa fa-th mr-1"></i>
                            Count Of CELL Deactivated
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn bg-info btn-sm" data-widget="remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body" style="background-color: white;">
                            @include('line_chart')
                    </div>
                </section>
            <section class="col-lg-12">
                <div class="card gradient">
                    <div class="card-header no-border">
                        <h3 class="card-title">
                            <i class="fa fa-th mr-1"></i>
                            COUNT OF MML COMMAND USER BY TIME
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn bg-info btn-sm" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn bg-info btn-sm" data-widget="remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body" style="background-color: white;">
                            @include('user_activity_chart')
                    </div>
                </section>

            </div>
        </div>
        </div>

  <footer class="main-footer">
    <strong style="font-size: 12px">Copyright &copy; 2018 <a href="https://www.telkomsel.com">Telkomsel</a>.</strong>
        <div class="float-right d-none d-sm-inline-block">
            <b>CHANGE
            </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
 <script src="{{url('')}}/js/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{url('')}}/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="{{url('')}}/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{url('')}}/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{url('')}}/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="{{url('')}}/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{url('')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="{{url('')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{url('')}}/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{url('')}}/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{url('')}}/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url('')}}/dist/js/demo.js"></script>
<script>
$("#nav-open").click(function(){
    if (document.getElementById("wrapper").style.padding == "0px") {
        $("#wrapper").css('padding-left', '150px');
        $("#sidebar-wrapper").css('width', '150px');
    }else {
        document.getElementById("wrapper").style.padding = "0";
        document.getElementById("sidebar-wrapper").style.width = "0";
    }
});

$(document).ready(function(){
    $.ajax({
        url:'{{url("dafinci")}}',
        type:'get',
        success: function(result){
            $('#dafinci').html(result);
            $('#dafinci_change').html('Dafinci&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
        }
    });
    $.ajax({
        url:'{{url("remedy")}}',
        type:'get',
        success: function(result){
            $('#remedy').html(result);
            $('#remedy_change').html('Remedy&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
        }
    });
        $.ajax({
        url:'{{url("add_bts")}}',
        type:'get',
        success: function(result){
            $('#add_bts').html(result);
            $('#change_add_bts').html('Change&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
        }
    });
    $.ajax({
        url:'{{url("btsonair")}}',
        type:'get',
        success: function(result){
            $('#btsonair').html(result);
            $('#btsonair_change').html('On Air&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
        }
    });
    $.ajax({
        url:'{{url("nodin")}}',
        type:'get',
        success: function(result){
            $('#nodin').html(result);
            $('#nodin_change').html('Nodin&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
        }
    });

    $.ajax({
        url:'{{url("act_cell")}}',
        type:'get',
        success: function(result){
            $('#act_cell').html(result);
        }
    });

    $.ajax({
        url:'{{url("dea_cell")}}',
        type:'get',
        success: function(result){
            $('#dea_cell').html(result);
        }
    });

    $.ajax({
        url:'{{url("act_license")}}',
        type:'get',
        success: function(result){
            $('#act_license').html(result);
        }
    });

    $.ajax({
        url:'{{url("dea_license")}}',
        type:'get',
        success: function(result){
            $('#dea_license').html(result);
        }
    });

    $.ajax({
        url:'{{url("add_license")}}',
        type:'get',
        success: function(result){
            $('#license').html(result);
            $('#license_change').html('License&nbsp;<i class="fa" style="color:green">&#xf062;</i>'+result);
        }
    });

});
</script>
</body>
</html>
